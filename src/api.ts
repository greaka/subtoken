export async function getEndpoints() {
    const res = await fetch(`https://api.guildwars2.com/v2.json?v=latest`);
    let raw = await res.json();
    if (!res.ok) {
        throw new Error(raw.text);
    }
    let routes: Route[] = raw.routes;
    routes = routes.filter(route => route.active && route.auth);
    return { urls: routes.map(route => route.path) };
}

type Route = {
    path: string;
    lang: boolean;
    auth: boolean;
    active: boolean;
};

export async function getTokenInfo(apiKey: string): Promise<TokenInfo> {
    const res = await fetch(`https://api.guildwars2.com/v2/tokeninfo?v=2023-04-02T00:00:00Z&access_token=` + apiKey);
    let raw = await res.json();
    if (!res.ok) {
        throw new Error(raw.text);
    }
    let info: TokenInfo = raw;
    return info;
}

export async function getAccount(apiKey: string): Promise<Account> {
    const res = await fetch(`https://api.guildwars2.com/v2/account?v=2023-04-02T00:00:00Z&access_token=` + apiKey);
    let raw = await res.json();
    if (!res.ok) {
        throw new Error(raw.text);
    }
    let info: Account = raw;
    return info;
}

export async function getWorlds(): Promise<World[]> {
    const res = await fetch(`https://api.guildwars2.com/v2/worlds?ids=all&v=2023-04-02T00:00:00Z`);
    let raw = await res.json();
    if (!res.ok) {
        throw new Error(raw.text);
    }
    return raw;
}

export type World = {
    id: number;
    name: string;
    population: string;
}

export type TokenInfo = {
    id: string;
    name: string;
    permissions: string[];
    type: string;
    expires_at: string | undefined;
    issued_at: string | undefined;
    urls: string[] | undefined;
}

export type Account = {
    id: string;
    name: string;
    age: number;
    last_modified: string;
    world: number;
    guilds: string[];
    created: string;
    access: string[];
    commander: boolean;
    fractal_level: number | undefined;
    daily_ap: number | undefined;
    monthly_ap: number | undefined;
    wvw_rank: number | undefined;
}
